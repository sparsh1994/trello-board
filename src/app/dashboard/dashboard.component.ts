import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BoardService } from '../board/board.service';
import { Board } from '../board/board'

@Component({
  selector: 'trello-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  boards: Board[];

  constructor(private boardService: BoardService,
    private router: Router) { }

  ngOnInit() {
    this.boards = [];
    this.boardService.getAll().subscribe((boards: Board[]) => {
      this.boards = boards;
    });
    setTimeout(function () {
      document.getElementById('content-wrapper').style.backgroundColor = "#fff";
    }, 100);
  }

  public addBoard() {
    console.log('Adding new board');
    this.boardService.post(<Board>{ title: "New board" })
      .subscribe((board: Board) => {
        this.router.navigate(['/board', board._id]);
        console.log('new board added');
      });
  }

}