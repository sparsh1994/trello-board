import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '../httpclient';
import { Card } from '../card/card';

@Injectable()
export class CardService {
  apiUrl = '/card';

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get(this.apiUrl)
      .map(res => res.json());
  }

  get(id: string) {
    return this.http.get(this.apiUrl + '/' + id)
      .map(res => res.json());
  }

  put(card: Card) {
    return this.http.put(this.apiUrl + '/' + card._id, JSON.stringify(card))
      .map(res => <Card>res.json().data);
  }

  post(card: Card) {
    return this.http.post(this.apiUrl, JSON.stringify(card))
      .map(res => <Card>res.json().data);
  }

  delete(card: Card) {
    return this.http.delete(this.apiUrl + '/' + card._id)
      .map(res => <Card>res.json().data);
  }

}
