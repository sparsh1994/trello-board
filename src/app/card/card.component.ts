import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ChangeDetectorRef, NgZone } from '@angular/core';
import { Card } from './card';
import { Column } from '../column/column';
import { CardService } from './card.service';
import { TitleCasePipe } from '../pipes/titlecase.pipe';

declare var $: any;
@Component({
  selector: 'board-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() card: Card;
  @Output() cardUpdate: EventEmitter<Card>;
  @Output() cardDelete: EventEmitter<Card>;
  editingCard = false;
  currentTitle: string;
  zone: NgZone;
  newComment: string;
  canEditTitle: boolean = false;
  showModal = false;
  constructor(private el: ElementRef,
    private ref: ChangeDetectorRef,
    private cardService: CardService) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.cardUpdate = new EventEmitter();
    this.cardDelete = new EventEmitter();
  }

  ngOnInit() { }

  deleteCard() {
    this.cardDelete.emit(this.card);
  }

  clearComment() {
    this.newComment = '';
  }

  openModal(data) {
    console.log(data)
    this.card = data;
    this.showModal = true;
    $('#modal-form').dialog({ modal: true });
  }

  closeModal() {
    this.showModal = false;
  }

  addComment() {
    console.log(this.newComment)
    if (!this.card.comments) {
      this.card.comments = [];
    }
    this.card.comments.push(this.newComment);
    console.log(this.card.comments)
    this.cardService.put(this.card).subscribe(res => {
      this.card = res;
      console.log(this.card)
    });
  }

  editTitle() {
    console.log('edit title called')
    this.canEditTitle = !this.canEditTitle;
    console.log(this.canEditTitle)
  }

  blurOnEnter(event) {
    if (event.keyCode === 13) {
      event.target.blur();
    } else if (event.keyCode === 27) {
      this.card.title = this.currentTitle;
      this.editingCard = false;
    }
  }

  editCard() {
    this.editingCard = true;
    this.currentTitle = this.card.title;
    console.log(this.currentTitle)
    let textArea = this.el.nativeElement.getElementsByTagName('textarea')[0];

    setTimeout(function () {
      textArea.focus();
    }, 0);
  }

  updateCard() {
    if (!this.card.title || this.card.title.trim() === '') {
      this.card.title = this.currentTitle;
    }

    this.cardService.put(this.card).subscribe(res => {
      console.log(res, 'called')
      this.card = res;
    });
    this.editingCard = false;
  }

  //TODO: check lifecycle
  private ngOnDestroy() {
  }

}